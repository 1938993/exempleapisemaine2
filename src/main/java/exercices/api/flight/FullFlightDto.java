package exercices.api.flight;

public class FullFlightDto {

    public final String id;
    public final String number;
    public final String destination;
    public final String company;
    public final Long numberPassengers;
    public final Boolean isFull;

    public FullFlightDto(String id, String number, String destination, String company, Long numberPassengers, Boolean isFull) {
        this.id = id;
        this.number = number;
        this.destination = destination;
        this.company = company;
        this.numberPassengers = numberPassengers;
        this.isFull = isFull;
    }
}
