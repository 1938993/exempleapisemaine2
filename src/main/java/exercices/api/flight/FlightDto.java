package exercices.api.flight;

public class FlightDto {

    public final String id;
    public final String number;
    public final String destination;
    public final String company;

    public FlightDto(String id, String number, String destination, String company) {
        this.id = id;
        this.number = number;
        this.destination = destination;
        this.company = company;
    }
}
