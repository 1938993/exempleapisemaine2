package exercices.api.flight;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 * Étape 2: On ajoute les routes.  Pour cette ressource, l'url commencera par /flights pour toutes les
 * méthodes.  Comme c'est commun à tous, on peut indiquer cette route au niveau de la classe, en utili-
 * sant l'annotation @RequestMapping.
 * 
 * Au niveau d'une ressource, il faut toujours s'assurer que l'url est unique pour un verbe HTTP.  On peut
 * donc avoir un @GetMapping et @PostMapping sur /flights, mais on ne peut avoir deux @GetMapping sur /flight.
 * D'ailleurs, si vous faites cette erreur, Spring vous informera de l'ambiguité au démarrage de votre
 * application.
 * 
 * Attention!  Le nom des routes est toujours un nom, au pluriel, en minuscules et commençant par le /.
 */
@RequestMapping("/flights")
public interface FlightResource2 {

    /*
     * Le verbe GET sur la route "/flights".  Rien n'est ajouté à cette route.
     */
    @GetMapping
    List<FullFlightDto> get();

    /*
     * Le verbe GET sur la route "/flights/{id}".  L'ajout du /{id} permet de désigner une deuxième route,
     * avec le verbe GET.  Comme la ressource à atteindre est une variable, on utilise les { } pour le désigner.
     * Quand la route sera appelée, ce sera /flights/1 ou /flights/2 et la valeur de 'id' sera récupérée dans
     * l'url.
     */
    @GetMapping("/{id}")
    FlightDto get(String id);

    /*
     * Le verbe GET sur la route "/flights/search".  C'est une troisième route avec le verbe GET, l'url doit
     * donc être différente des deux autres routes.  Elle n'a pas besoin d'être une variable, alors on la 
     * laisse en constante.
     */
    @GetMapping("/search")
    List<FlightDto> search(String company, Boolean isFull);

    /*
     * La création sur la route /flights.  Elle est différente de la première route, parce qu'on utilise le verbe
     * HTTP POST, ici.
     */
    @PostMapping
    FullFlightDto create(FullFlightDtoWithoutId flight);

    /*
     * Le verbe PUT sur la route "/flights/{id}".  On ajoute l'id, ici, parce qu'on modifie toujours une seule
     * ressource 'flights' à la fois.  La route /flights tout court représente l'ensemble (ou la collection) de
     * toutes les ressources flights.
     */
    @PutMapping("/{id}")
    void update(String id, FullFlightDto flight);

    /*
     * Le verbe DELETE sur la route "/flights/{id}".
     */
    @DeleteMapping("/{id}")
    void delete(String id);
}
