package exercices.api.flight;

import java.util.List;

/*
 * Étape 1: Création de l'interface avec les méthodes spécifiées par le client.  Ici, on se concentre d'abord
 * sur les opérations CRUD (create, read, update et delete).  Les opérations plus complexes viendront plus
 * tard, mais elles sont créées exactement de la même manière au niveau de la ressource.
 * 
 * La convention, dans une ressource, est de commencer par les méthodes en lecture, suivies des méthodes de 
 * modification.  Je mets l'ordre 'create, update, delete' pour les méthodes, puisque c'est un cycle de vie
 * normal pour une ressource. 
 * 
 * Attention!  Si vous codez en Anglais, 'Resource' prend un seul 's'.
 */
public interface FlightResource1 {

    List<FullFlightDto> get();

    FlightDto get(String id);

    List<FlightDto> search(String company, Boolean isFull);

    /*
     * À la création, on retourne l'objet créé puisqu'on lui a ajouté un identifiant.  Il est donc plus facile, pour
     * une application client, de gérer ce nouvel objet, par exemple, pour l'ajouter à la collection d'objets affichés.
     */
    FullFlightDto create(FullFlightDtoWithoutId flight);

    /*
     * Même si le DTO contient un identifiant, il est conventionnel de donner cet identifiant de façon séparée.  Il 
     * faudra, éventuellement, faire une validation (l'id donné correspond à l'id du DTO donné).
     */
    void update(String id, FullFlightDto flight);

    void delete(String id);
}
