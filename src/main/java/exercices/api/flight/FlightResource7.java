package exercices.api.flight;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/*
 * Étape 7: Les statuts HTTP, qui accompagne les retours de données au client.  Ce sont les codes 200, 201, 204, 400, etc.
 * 
 * Par défaut, lors d'un succès, le code 200 est retourné.  Cependant, à la création, on est supposé retourné un code
 * 201 - Created.  Aussi, lorsqu'une méthode ne retourne rien (void), le corps de la réponse n'aura aucun contenu.  La
 * convention veut donc qu'on signifie au client que c'est voulu, en retournant un statut HTTP 204 - No Content.
 * 
 * Chaque méthode peut retourner un statut différent, l'annotation va donc être ajoutée pour chaque méthode.
 */
@RequestMapping(value = FlightResource7.RESOURCE_PATH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public interface FlightResource7 {

    String RESOURCE_PATH = "/flights";
    String PATH_FOR_SEARCH = "/search";
    String PARAM_ID = "id";
    String PARAM_COMPANY = "company";
    String PARAM_DESTINATION = "destination";
    String PATH_WITH_ID = "/{" + PARAM_ID + "}";

    /* Un GET retourne normalement un 200 - OK.  Comme c'est la valeur par défaut, ce n'est pas obligatoire de le 
     * spécifier.  On peut également le mettre pour une meilleure lisibilité. */
    @GetMapping
    List<FullFlightDto> get();

    @GetMapping(PATH_WITH_ID)
    FlightDto get(@PathVariable(PARAM_ID) String id);

    @GetMapping(FlightResource7.PATH_FOR_SEARCH)
    @ResponseStatus(HttpStatus.OK)
    List<FlightDto> search(@RequestParam(PARAM_COMPANY) String company, @RequestParam(PARAM_DESTINATION) String destination);

    /* À la création, la convention dicte un statut HTTP 201 - Created. On utilise l'annotation @ResponseStatus, avec
     * entre parenthèses, le code à retourner. 
     * 
     * L'ordre des annotation est le mapping d'abord, le code de réponse ensuite (pour respecter l'ordre requête-
     * réponse).
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    FullFlightDto create(@RequestBody FullFlightDtoWithoutId flight);

    /*
     * Pour toutes les méthodes void (qu'elles soient PUT, POST, DELETE ou autre), le code conventionnel est 204 -
     * No Content.  C'est la même annotation, avec le code différent 204 - No Content.
     */
    @PutMapping(PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void update(@PathVariable(PARAM_ID) String id, @RequestBody FullFlightDto flight);

    @DeleteMapping(PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable(PARAM_ID) String id);
}
