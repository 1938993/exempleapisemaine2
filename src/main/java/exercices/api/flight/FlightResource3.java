package exercices.api.flight;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 * Étape 3: Réusinage!  On extrait les contantes magiques!
 */
@RequestMapping(FlightResource3.RESOURCE_PATH)
public interface FlightResource3 {

    String RESOURCE_PATH = "/flights";
    String PATH_FOR_SEARCH = "/search";
    String PATH_WITH_ID = "/{id}";

    @GetMapping
    List<FullFlightDto> get();

    @GetMapping(PATH_WITH_ID)
    FlightDto get(String id);

    @GetMapping(FlightResource3.PATH_FOR_SEARCH)
    List<FlightDto> search(String company, Boolean isFull);

    @PostMapping
    FullFlightDto create(FullFlightDtoWithoutId flight);

    @PutMapping(PATH_WITH_ID)
    void update(String id, FullFlightDto flight);

    @DeleteMapping(PATH_WITH_ID)
    void delete(String id);
}
