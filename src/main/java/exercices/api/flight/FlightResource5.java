package exercices.api.flight;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 * Étape 5: Réusinage!  On extrait les constantes magiques.
 */
@RequestMapping(FlightResource5.RESOURCE_PATH)
@ResponseBody
public interface FlightResource5 {

    String RESOURCE_PATH = "/flights";
    String PATH_FOR_SEARCH = "/search";
    /* On ajoute le paramètre pour l'identifiant. */
    String PARAM_ID = "id";
    String PARAM_COMPANY = "company";
    String PARAM_DESTINATION = "destination";
    /* Comme c'est important d'avoir la même syntaxe, on utilise notre param_id ici aussi! */
    String PATH_WITH_ID = "/{" + PARAM_ID + "}";

    @GetMapping
    List<FullFlightDto> get();

    @GetMapping(PATH_WITH_ID)
    /* On remplace la string "id" par sa variable dans l'annotation. */
    FlightDto get(@PathVariable(PARAM_ID) String id);

    @GetMapping(FlightResource5.PATH_FOR_SEARCH)
    List<FlightDto> search(@RequestParam(PARAM_COMPANY) String company, @RequestParam(PARAM_DESTINATION) String destination);

    @PostMapping
    FullFlightDto create(@RequestBody FullFlightDtoWithoutId flight);

    @PutMapping(PATH_WITH_ID)
    void update(@PathVariable(PARAM_ID) String id, @RequestBody FullFlightDto flight);

    @DeleteMapping(PATH_WITH_ID)
    void delete(@PathVariable(PARAM_ID) String id);
}
