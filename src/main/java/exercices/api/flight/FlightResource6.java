package exercices.api.flight;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 * Étape 6: Spécification du format échangé.  Notre API Rest est basé sur le JSON, et bien que ce soit
 * le format par défaut du cadriciel Spring, il est important de restreindre les autres formats.  En effet,
 * certaines attaques informatiques sont basées sur le type de format passé à l'API.
 * 
 * Notre API accepte un seul format, que nous allons autorisé, et par le fait même, nous allons refuser tous les
 * autres.  Ce principe, le 'white-listing', permet donc un accès à ce qui est connu seulement.  C'est plus facile
 * d'autoriser quelque chose (et d'agrandir les permission) que de faire le contraire (black-listing) qui consiste
 * à interdire une chose et laisser le reste passer.
 * 
 * Comme toutes les méthodes veulent recevoir et/ou renvoyer du JSON, on va le spécifier au niveau de la classe,
 * en ajoutant des paramètres à l'annotation @RequestMapping.  Les paramètres d'une annotation ressemblent aux
 * 'props' en HTML ou Javascript.
 * 
 * Les propriétés sont 'consumes' et 'produces', avec la valeur MediaType.APPLICATION_JSON_VALUE.  Avec cet ajout,
 * il faut aussi spécifier la propriété de l'url (value).  On ne l'avait pas fait avant, car lorsqu'une seule valeur
 * est passée en paramètre, c'est par défaut la valeur de la route.  On place donc value= devant le nom de la route.
 * 
 * Attention!  Une fois ces paramètres ajoutés, il faudra peut-être modifié vos requêtes dans Postman pour ajouter
 * le 'Content-Type' et le 'Accept' pour qu'ils aient la valeur 'application/json'.
 */
@RequestMapping(value = FlightResource6.RESOURCE_PATH,
consumes = MediaType.APPLICATION_JSON_VALUE, 
produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public interface FlightResource6 {

    String RESOURCE_PATH = "/flights";
    String PATH_FOR_SEARCH = "/search";
    String PARAM_ID = "id";
    String PARAM_COMPANY = "company";
    String PARAM_DESTINATION = "destination";
    String PATH_WITH_ID = "/{" + PARAM_ID + "}";

    @GetMapping
    List<FullFlightDto> get();

    @GetMapping(PATH_WITH_ID)
    FlightDto get(@PathVariable(PARAM_ID) String id);

    @GetMapping(FlightResource6.PATH_FOR_SEARCH)
    List<FlightDto> search(@RequestParam(PARAM_COMPANY) String company, @RequestParam(PARAM_DESTINATION) String destination);

    @PostMapping
    FullFlightDto create(@RequestBody FullFlightDtoWithoutId flight);

    @PutMapping(PATH_WITH_ID)
    void update(@PathVariable(PARAM_ID) String id, @RequestBody FullFlightDto flight);

    @DeleteMapping(PATH_WITH_ID)
    void delete(@PathVariable(PARAM_ID) String id);
}
