package exercices.api.flight;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RequestMapping(value = FlightResourceFinal.RESOURCE_PATH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public interface FlightResourceFinal {

    String RESOURCE_PATH = "/flights";
    String PATH_FOR_SEARCH = "/search";
    String PARAM_ID = "id";
    String PARAM_COMPANY = "company";
    String PARAM_DESTINATION = "destination";
    String PATH_WITH_ID = "/{" + PARAM_ID + "}";

    @Operation(summary = "Obtient tous les vols disponibles.")
    @ApiResponses(value = { @ApiResponse(responseCode = "200",
            description = "Vols trouvés",
            content = { @Content(mediaType = "application/json", schema = @Schema(implementation = FlightDto.class)) }) })
    @GetMapping
    List<FullFlightDto> get();

    @Operation(summary = "Obtient un vold par son identifiant.")
    @ApiResponses(value = { @ApiResponse(responseCode = "200",
            description = "Vol trouvé",
            content = { @Content(mediaType = "application/json", schema = @Schema(implementation = FlightDto.class)) }), @ApiResponse(responseCode = "400",
                    description = "Identifiant invalide",
                    content = @Content), @ApiResponse(responseCode = "404", description = "Vol introuvable", content = @Content) })
    @GetMapping(PATH_WITH_ID)
    FlightDto get(@PathVariable(PARAM_ID) String id);

    @Operation(summary = "Obtient tous les vols correspondant aux critères de recherche.")
    @ApiResponses(value = { @ApiResponse(responseCode = "200",
            description = "Vols trouvés",
            content = { @Content(mediaType = "application/json", schema = @Schema(implementation = FlightDto.class)) }) })
    @GetMapping(FlightResourceFinal.PATH_FOR_SEARCH)
    List<FlightDto> search(@RequestParam(PARAM_COMPANY) String company, @RequestParam(PARAM_DESTINATION) String destination);

    @Operation(summary = "Crée un nouveau vol avec les informations données.")
    @ApiResponses(value = { @ApiResponse(responseCode = "201",
            description = "Vol créé",
            content = { @Content(mediaType = "application/json", schema = @Schema(implementation = FlightDto.class)) }), @ApiResponse(responseCode = "400",
                    description = "Vol invalide",
                    content = @Content), @ApiResponse(responseCode = "415", description = "Vol déjà existant", content = @Content) })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    FullFlightDto create(@RequestBody FullFlightDtoWithoutId flight);

    @Operation(summary = "Modifie un vol existant avec les informations données.")
    @ApiResponses(value = { @ApiResponse(responseCode = "204", description = "Vol créé", content = @Content), @ApiResponse(responseCode = "400",
            description = "Vol invalide",
            content = @Content), @ApiResponse(responseCode = "404", description = "Vol introuvable", content = @Content) })
    @PutMapping(PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void update(@PathVariable(PARAM_ID) String id, @RequestBody FullFlightDto flight);

    @Operation(summary = "Supprime un vol par son identifiant.")
    @ApiResponses(value = { @ApiResponse(responseCode = "204", description = "Vol supprimé", content = @Content), @ApiResponse(responseCode = "400",
            description = "Identifiant invalide",
            content = @Content), @ApiResponse(responseCode = "404", description = "Vol introuvable", content = @Content) })
    @DeleteMapping(PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable(PARAM_ID) String id);
}
