package exercices.api.flight;

public class FullFlightDtoWithoutId {

    public final String number;
    public final String destination;
    public final String company;
    public final Long numberPassengers;
    public final Boolean isFull;

    public FullFlightDtoWithoutId(String number, String destination, String company, Long numberPassengers, Boolean isFull) {
        this.number = number;
        this.destination = destination;
        this.company = company;
        this.numberPassengers = numberPassengers;
        this.isFull = isFull;
    }
}
