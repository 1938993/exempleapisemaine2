package exercices.api.flight;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 * Étape 4: Ajout des paramètres et de la mécanique d'échange des informations.  C'est à cette
 * étape qu'on indique à Spring comment transformer la requête HTTP reçue en information organisée
 * et utilisable par notre application.
 * 
 * Première chose, même si certaines méthodes ne retournent rien (void), toutes les routes doivent
 * retournées une réponse.  On indique cela par l'annotation @ResponseBody au niveau de la classe,
 * pour éviter d'avoir à la répéter pour chaque méthode.
 */
@RequestMapping(FlightResource4.RESOURCE_PATH)
@ResponseBody
public interface FlightResource4 {

    String RESOURCE_PATH = "/flights";
    String PATH_FOR_SEARCH = "/search";
    String PATH_WITH_ID = "/{id}";

    /*
     * Cette méthode n'a pas de paramètres, elle reste donc telle quelle.
     */
    @GetMapping
    List<FullFlightDto> get();

    /*
     * Le 'getById' a un paramètre 'id', qui viendra du chemin de la route utilisée (comme dans 
     * la route /flights/1, où le 1 est le paramètre).  Il faut donc indiquer à Spring comment prendre
     * le paramètre dans la route et le passer au paramètre String id.
     * 
     * Pour ce faire, on utilise l'annotation @PathVariable, avec le nom de la variable.
     */
    @GetMapping(PATH_WITH_ID)
    FlightDto get(@PathVariable("id") String id);

    /*
     * Une autre façon de passer des paramètres par l'url est d'utiliser les paramètres de l'url, comme dans
     * la route suivante: /flights/search?company=AirCanada&destination=Cancun.  Tout ce qui vient après le ?
     * sont des paramètres.  Chaque paramètre est séparé par un & et représente une clé=valeur.
     * 
     * Pour indiquer à Spring d'aller chercher les paramètres dans ce qu'on appelle la 'queryString', on utilise
     * l'annotation @RequestParam avec le nom du paramètre dans l'url.
     */
    @GetMapping(FlightResource4.PATH_FOR_SEARCH)
    List<FlightDto> search(@RequestParam("company") String company, @RequestParam("destination") String destination);

    /*
     * Dans un POST (ou un PUT), on ne peut utiliser les paramètres de l'url (la 'queryString') parce que c'est une
     * méthode peu sécurisée pour passer des paramètres sensibles (comme un mot de passe ou un numéro de carte de
     * crédit).  De plus, les POST et PUT ont généralement beaucoup plus d'informations à gérer qu'un ou deux paramètres.
     * 
     * On utilise donc le corps de la requête HTTP, avec un format JSON, pour passer cette information.  Encore une
     * fois, il faut le spécifier à notre cadriciel Spring, en utilisant l'annotation @RequestBody.  Cela signifie que
     * l'information pour la création de la ressource souhaitée est dans le corps de la requête.
     */
    @PostMapping
    FullFlightDto create(@RequestBody FullFlightDtoWithoutId flight);

    /*
     * Par convention, le PUT est une combinaison du 'getById' pour le paramètre 'id' et du POST (par le corps de la
     * requête).
     */
    @PutMapping(PATH_WITH_ID)
    void update(@PathVariable("id") String id, @RequestBody FullFlightDto flight);

    /*
     * Le DELETE est en tout point identique au 'getById': un seul paramètre, 'id', à récupérer dans l'url de la route.
     */
    @DeleteMapping(PATH_WITH_ID)
    void delete(@PathVariable("id") String id);
}
