# Notes de cours

Ce projet est destiné à servir d'exemple pour la construction d'un API (semaine 2). Ce n'est pas un projet destiné à être rouler comme une application fonctionnelle (aucune implémentation), bien qu'il sera toujours possible de le rouler.

## Installation

Faites un clone du projet sur votre poste, afin de vous guider dans la réalisation de votre exercice de la semaine 2.

## Contributing
Étant donné la nature du projet, aucune contribution ne sera acceptée.

## License
[GNU](https://choosealicense.com/licenses/gpl-3.0/)
